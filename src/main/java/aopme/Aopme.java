package aopme;

import aopme.biz.Bean1;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;

import java.util.function.Supplier;

import static java.util.stream.IntStream.rangeClosed;

@Configuration
@ComponentScan
@EnableAspectJAutoProxy
@PropertySource("application.properties")
@Slf4j(topic = "aop_me")
class Aopme {
    public static void main(String[] args) {
        BeanFactory ctx = new AnnotationConfigApplicationContext(Aopme.class);
        Bean1 bean1 = ctx.getBean(Bean1.class);
        rangeClosed(1, 10)
                .mapToObj(bean1::ping)
                .forEach(LOG::info);
    }

    @Bean
    Supplier<Long> ticks() {
        return System::nanoTime;
    }
}
