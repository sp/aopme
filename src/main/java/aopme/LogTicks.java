package aopme;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

@Aspect
@Component
@RequiredArgsConstructor
@Slf4j(topic = "log_ticks")
class LogTicks {
    private final Supplier<Long> ticks;
    @Value("${ticks.delay}")
    private final long delay;

    @Around("execution(* aopme.biz.*.*(..))")
    private Object do_(ProceedingJoinPoint joinPoint) throws Throwable {
        long a = ticks.get();
        TimeUnit.MILLISECONDS.sleep(delay);
        Object result = joinPoint.proceed();
        long b = ticks.get();
        LOG.info("{} took {} ticks", joinPoint, b - a);
        return result;
    }
}
