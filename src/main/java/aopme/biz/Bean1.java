package aopme.biz;

import org.springframework.stereotype.Component;

@Component
public class Bean1 {
    public String ping(int x) {
        return "pong " + x;
    }
}
